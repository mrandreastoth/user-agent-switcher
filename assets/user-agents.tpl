?# Note that most of the queries below work by extracting the „preferred item“
?# from WikiData. If the latest version is therefor not marked as „preferred“
?# then that version will not be used even if it is newer than the current
?# preferred version entry.
?#
#Note: Browser version data is semi-regularily updated based on the version data
#      available through WikiData's SPARQL data service
#      (See `assets/user-agents.tpl` in the source for the queries involved.)
?# Latest Firefox version on Windows 10 and Ubuntu 18.04LTS x86-64
?=firefox as version[split(".")]
?+	SELECT
?+		?version
?+	WHERE {
?+		wd:Q698 wdt:P348 ?version.
?+	}
Windows / Firefox {firefox[0]} [Desktop]: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:{firefox[0:2][join(".")]}) Gecko/20100101 Firefox/{firefox[0:2][join(".")]}
Linux / Firefox {firefox[0]} [Desktop]: Mozilla/5.0 (X11; Linux x86_64; rv:{firefox[0:2][join(".")]}) Gecko/20100101 Firefox/{firefox[0:2][join(".")]}
?#
?# Latest Desktop Safari version on latest macOS x86-64
?#XXX: Cannot automatically update Safari version as each version ships with
?#     a different WebKit version token set.
?=macos_stru as version[split(".")][join("_")]
?+	SELECT
?+		?version
?+	WHERE {
?+		wd:Q14116 wdt:P348 ?version.
?+	}
Mac OS X / Safari 11.1.1 [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X {macos_stru}) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1.1 Safari/605.1.15
?#
?# Latest Internet Explorer version on Windows 7 x86-64
?# - Frozen, since there are no new releases of this anymore.
Windows / IE 11 [Desktop]: Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko
?#
?# Latest Microsoft Edge version on Windows 10 x86-64
?# – Actually the Edge version is not actually listed here; only EdgeHTML.
?#XXX: Cannot automatically update EdgeHTML version as each version ships with
?#     a different Chrome version token set.
?#?=edgehtml as version[split(".")]
?#?+	SELECT
?#?+		?version
?#?+	WHERE {
?#?+		wd:Q19668903 wdt:P348 ?version.
?#?+	}
Windows / Edge 17 [Desktop]: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134
?#
?# Latest Google Chrome version on Windows 10 x86-64
?# – The Safari and WebKit tokens are frozen and therefor irrelevant.
?=chrome as version[split(".")]
?+	SELECT
?+		?version
?+	WHERE {
?+		wd:Q777 wdt:P348 ?version.
?+	}
Windows / Chrome {chrome[0]} [Desktop]: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/{chrome[join(".")]} Safari/537.36
?#
?# Latest Firefox ESR version on Windows 10 x86-64
?=firefox_esr as version[split()][0][split(".")]
?+	SELECT
?+		?version
?+	WHERE {
?+		wd:Q698 p:P348 ?versionStmt.
?+		?versionStmt ps:P348 ?version.
?+		?versionStmt pq:P548 wd:Q15726348.
?+	}
?+	ORDER BY
?+		DESC(?version)
?+	LIMIT
?+		1
Windows / Firefox {firefox_esr[0]} ESR [Desktop]: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:{firefox_esr[0:2][join(".")]}) Gecko/20100101 Firefox/{firefox_esr[0:2][join(".")]}

?# Latest Mobile Firefox and Chrome versions on latest Android aarch32
?=android_str as version
?+	SELECT
?+		?version
?+	WHERE {
?+		wd:Q94 wdt:P348 ?version.
?+	}
Android Phone / Firefox {firefox[0]} [Mobile]: Mozilla/5.0 (Android {android_str}; Mobile; rv:{firefox[0:2][join(".")]}) Gecko/{firefox[0:2][join(".")]} Firefox/{firefox[0:2][join(".")]}
Android Phone / Chrome {chrome[0]} [Mobile]: Mozilla/5.0 (Linux; Android {android_str}; Z832 Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/{chrome[join(".")]} Mobile Safari/537.36
Android Tablet / Chrome {chrome[0]} [Mobile]: Mozilla/5.0 (Linux; Android {android_str}; SAMSUNG-SM-T377A Build/NMF26X) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/{chrome[join(".")]} Mobile Safari/537.36
?#
?# Latest Mobile Safari version on latest iOS aarch64
?#XXX: Cannot automatically update Safari version as each version ships with
?#     a different WebKit version token set.
?=ios_stru as version[split(".")][join("_")]
?+	SELECT
?+		?version
?+	WHERE {
?+		wd:Q14116 wdt:P348 ?version.
?+	}
iPhone / Safari 11.1.1 [Mobile]: Mozilla/5.0 (iPhone; CPU OS {ios_stru} like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1.1 Mobile/14E304 Safari/605.1.15
iPad / Safari 11.1.1 [Mobile]: Mozilla/5.0 (iPad; CPU OS {ios_stru} like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1.1 Mobile/15E148 Safari/605.1.15

?# GoogleBot web crawler
?# - While GoogleBot is continually being developed this string is afaik frozen.
Google Bot [Bot]: Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)
?#
?# Latest PlayStation 4 stock browser
PS4 [Other]: Mozilla/5.0 (PlayStation 4 4.71) AppleWebKit/601.2 (KHTML, like Gecko)
?#
?# Latest CURL and WGet version on Linux x86-64
?=curl_str as version
?+	SELECT
?+		?version
?+	WHERE {
?+		wd:Q286306 wdt:P348 ?version.
?+	}
Curl [Other]: curl/{curl_str}
?=wget_str as version
?+	SELECT
?+		?version
?+	WHERE {
?+		wd:Q535461 wdt:P348 ?version.
?+	}
Wget [Other]: Wget/{wget_str} (linux-gnu)
